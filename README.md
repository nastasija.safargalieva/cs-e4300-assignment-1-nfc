###### CS-E4300 Network security, 2018

# Project Report: Ticket Application by Group 10

Buse Atli, Adriaan Knapen and Zoltán Tudlik

## Overview
An android application designed to read and write for multi-ride carousel ticket on an NFC cards. NXP MIFARE Ultralight C smart card (the ticket) is first authenticated and registered with certain amount of rides by a certified personnel before the actual usage. After the ticket is used for the first time will the ticket be valid for the remainder of the day. Each time the customer uses the ticket, the ride count is incremented only if the ticket is still valid and the maximum ride count is not reached yet. A customer can buy additional rides after a successful validity and ride count check. The used ticket can be returned anytime and re-issued for next costumers.

## Ticket application structure

### Memory management

<table dir="auto">
<tbody>
<tr>
<td colspan="2"><strong> Page address</strong></td>
<td colspan="4"><strong> Byte number</strong></td>
</tr>
<tr>
<td><strong> Decimal</strong></td>
<td><strong> Hex</strong></td>
<td><strong> 0</strong></td>
<td><strong> 1</strong></td>
<td><strong> 2</strong></td>
<td><strong> 3</strong></td>
</tr>
<tr>
<td><strong>0</strong></td>
<td><strong>00h</strong></td>
<td colspan="4" style="background-color: #ccc">serial number</td>
</tr>
<tr>
<td><strong>1</strong></td>
<td><strong>01h</strong></td>
<td colspan="4" style="background-color: #ccc">serial number</td>
</tr>
<tr>
<td><strong>2</strong></td>
<td><strong>02h</strong></td>
<td style="background-color: #ccc">serial number</td>
<td>internal</td>
<td>lock bytes</td>
<td>lock bytes</td>
</tr>
<tr>
<td><strong>4</strong></td>
<td><strong>04h</strong></td>
<td style="background-color: #C63D6D">max rides</td>
<td style="background-color: #C63D6D">max rides</td>
<td style="background-color: #C63D6D">until valid date</td>
<td style="background-color: #C63D6D">until valid date</td>
</tr>
<tr>
<td><strong>5</strong></td>
<td><strong>05h</strong></td>
<td style="background-color: #FF4F8D">mac max rides</td>
<td style="background-color: #FF4F8D">mac max rides</td>
<td style="background-color: #FF4F8D">mac max rides</td>
<td style="background-color: #FF4F8D">mac max rides</td>
</tr>
<tr>
<td><strong>6</strong></td>
<td><strong>06h</strong></td>
<td style="background-color: #40797A">usable until date</td>
<td style="background-color: #40797A">usable until date</td>
<td style="background-color: #5DB1B2">mac u.u. date</td>
<td style="background-color: #5DB1B2">mac u.u. date</td>
</tr>
<tr>
<td><strong>7</strong></td>
<td><strong>07h</strong></td>
<td style="background-color: #FF6EA1">m.r. &amp; d. mac backup</td>
<td style="background-color: #FF6EA1">m.r. &amp; d. mac backup</td>
<td style="background-color: #FF6EA1">m.r. &amp; d. mac backup</td>
<td style="background-color: #FF6EA1">m.r. &amp; d. mac backup</td>
</tr>
<tr>
<td><strong>41</strong></td>
<td><strong>29h</strong></td>
<td style="background-color: #F29724">16-bit counter</td>
<td style="background-color: #F29724">16-bit counter</td>
<td>-</td>
<td>-</td>
</tr>
<tr>
<td><strong>42</strong></td>
<td><strong>2Ah</strong></td>
<td colspan="4" style="background-color: #452700; color: #fff">authentication configuration = <em>{0x03, 0x00, 0x00, 0x00}</em></td>
</tr>
<tr>
<td><strong>43</strong></td>
<td><strong>2Bh</strong></td>
<td colspan="4" style="background-color: #452700; color: #fff">authentication configuration = <em>{0x00, 0x00, 0x00, 0x00}</em></td>
</tr>
<tr>
<td><strong>44 to 47</strong></td>
<td><strong>2Ch to 2Fh</strong></td>
<td colspan="4" style="background-color: #452700; color: #fff">authentication key = <em>mac_m(serial_number)</em></td>
</tr>
</tbody>
</table>

### States of the card

The card can be in one of the following states:

![](https://i.imgur.com/cxtF8Qk.png)

*The images displays the state of the card in Petrinet notation, the circles represent the "state" and the squares "actions", the remained is used to track the memory on the ticket and ensure that only valid actions can be taken. Which in turn can be used to animate and validate the model. The model itself can be found in the Gitlab repository as `statespace-ticket.cpn` and opened with CPN Tools v4.0.1.*

*The 5 and 100 are magic constants, 5 being the amount with which the ride count is always incremented and 100 the maximum ride count which can be loaded on the card.*

## Key management

There is one master key $`m`$ used for card authentication and signing the data on the card.

The authentication key of a card with serial number $`s`$ is set to $`hmac_m(s)`$. The data on the card itself is not encrypted and merely secured by adding MACs to all relevant sections. Two types of MACs are stored on the cards memory:

* *Rides mac*: $`hmac_m(s||max~rides||)`$
* *Date mac*: $`hmac_m(s||date)`$

## Implementation

Nearly the entire implementation is done in `com/example/auth/ticket/Ticket.java`. Some minor modifications where made to `com/example/auth/app/fragments/EmulatorFragment.java` in order to let it retrieve the validity data (amount of rides, dates, and so on) after the `issue` or `use` functions in the `Ticket` class where called.

Both the `issue` and `use` function do all of the read and write operations in a liniar fasion with a fail fast approach in mind. Thus if reading or writing fails, then the process will be terminated, the user is informed about the error and asked to repeat the action. The implementation is designed such that it is protected against tearing, meaning specifically that as long as writing of individual pages is an atomic operation, then the card will always be able to recover from interrupted user actions.

The authentication of both functions first reads the serial number and attempts to authenticate using a combination of this serial with the master key $`m`$. In case this fails, then a second authentication attempt will be made with the default key in order to check if this card is in it's initial state. When such situation arrises in the `use` function, then merely an error is displayed. For the `issue` function is first the authentication configuration written and then the authentication key. This order ensures that if the first step fails, then the card will still be recognized as un-initialized and thus the process will be repeated until both steps both succeed.

When the authentication is finished then the data will be read from the card, at first only page 4, 5, 6 and the 41 (counter) are read. This binary data is parsed to the `TicketData` subclass, which splits the binary data into individual variables and offers a `validate()` function to check if the ticket is valid.

`use` first retrieves the validitity of the ticket, if the ticket is valid then it will check if the `usable until date` variable was set (not `0x0`). If it is not already set, then it will write the current day to the first two bytes of page 6 and a MAC over this date to the second two bytes. Lastly is the counter incremented and on success a success message displayed to the user.

`issue` has three possible choices after checking the validity of the card:

1. The card is valid, thus the user still has remaining rides which haven't expired yet. Therefore we will attempt to increment the count of valid tickets. We first backup the MAC from page 5 to page 7, then update the MAC in page 5 and lastly update page 4 with the new amount of rides and until valid date.
2. The card is invalid, because the MAC of the amount of rides and until valid date is incorrect, however using the backup MAC would the card be still valid. This could possibly be the result of only writing the updated MAC but failing to write the actual incremented count and updated date in case 1. We restore this MAC by copying it to page 5 and retry the process.
3. The card is invalid and a new ticket should be issued. We clean the `usable until date` page and write all new data. No tearing can occur, as an interruption of the process will cause the card to be still invalid, thus on a retry will we still achieve the same end-result.

Dates are all stored in two bytes using a "days since Epoch"-timestamp. The choice for this implementation was made to decrease the amount of write operations, both in the `use` and `issue` action - although `use` could also have dropped the MAC for the `usable until date` which would have been less secure. The downside is that we now merely have a date and not a timestamp. This is sufficient for our current requirements, as the precision of the validity dates is only measured in days, thus we can assume that the ticket is valid until the end of the day of any date.


## Evaluation

### Security evaluation

Man in the middle attacks are always possible for the attacker, given that he/she has the appropriate hardware to engage in such an attack. However the unique authentication key between each card and the MACs signed with private keys make it hard to abuse this on a vast scale. The current protocol does not introduce any method to actively block such behavior, optionally could logging be employed to be able to detect fraud with post processing of these logs.

The cards do protect against duplication as they cannot be read without authentication. Thus it is not trivial to duplicate the issued cards.

It remains a major risk to keep use a single master key for all authentication and MACing. If one of the devices is compromised, then the key can be used to forge any card an attacker wants. Therefore it might be advisable to store the master key in a trusted environment and let that trusted environment be the only component to use the key.

### Reliability and deployability

The application supports two scenarios:

* **Issuing of new rides or incrementing rides** This is only done in scenarios where we are sure that tearing is not an issue, e.g. because it is done by certified personnel or the ticket is positioned securely in a ticket vending machine. Therefore there is no risk of tearing.

* **Use ticket** In this scenario do we want to spend one of the tickets, e.g. to open a security gate. This action requires two write operations when the ticket is still in the *Unused* state, however tearing is not an issue, since after the first write operation the ticket is still in a valid state - namely the *In Use* without any rides being consumed yet - and would require the user to present his/her card to the reader again.

Therefore the risk of tearing does not degrade the reliability of the system.

It should not be hard to deploy the system, as each machine using this source code can run with the same configuration.

## Final notes

The given framework was really useful, it allowed us to focus on the actual implementation instead of wasting a lot of our time on getting things to work which are completely unrelated to the course topic. The lecture on NFC was not that useful for this project, as we all had some basic knowledge on NFC and the lecture went quite early on into a lot of the hardware specifics of NFC - mostly features which are not even present on the MIFARE Ultralight C card we got.

Overall was it an enjoyable project, it's nice to have a project which actually works and see how "down to earth" implementing such solution actually is. In the end do we all feel that we got a good understanding how the public transport cards work, or at least consider them as much more than some black-box magical device which makes the card reader beep when we present the card.